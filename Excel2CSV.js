var numFiles = WScript.Arguments.Count();
if(numFiles==0){
  WScript.Quit();
}

var excel = new ActiveXObject("Excel.Application");
excel.DisplayAlerts = false;

for( var i=0; i<numFiles; i++ ){
  var file = WScript.Arguments.Item(i);
  var book = excel.Workbooks.Open(file);

  var fs = new ActiveXObject( "Scripting.FileSystemObject" );
  var baseName = fs.GetBaseName(file);
  var parentPath = fs.GetParentFolderName(file);

  var xlCSV = 6;
  book.SaveAs(parentPath+'/'+baseName+'.csv',xlCSV);
  book.Close();
}
excel.Quit();
excel = null;