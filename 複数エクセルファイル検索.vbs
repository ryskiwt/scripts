Dim Items
Set Items = WScript.Arguments
If Items.Count < 1 Then WScript.Quit

Dim SearchWord
SearchWord = InputBox("検索文字列を入力してください", "ファイル横断検索")
If SearchWord = "" Then WScript.Quit

Dim Excel
Set Excel = CreateObject("Excel.Application")
Excel.Visible = False
Excel.DisplayAlerts = Flase

Dim fs
Set fs = CreateObject("Scripting.FileSystemObject")
    
Dim Book
Set Book = Excel.Workbooks.Add
Dim Range
Set Range = Book.Worksheets(1).Range("A1")

Call WriteHeader()
Call Search(Items)

Excel.Visible = True


Sub Search(Files)
    Dim File
    For Each File In Files
        If fs.FolderExists(File) Then
            Call Search(fs.GetFolder(File).Files)
            Call Search(fs.GetFolder(File).SubFolders)
        Else
            Dim ExtName
            ExtName = fs.GetExtensionName(File)
            If InStr(ExtName,"xls") Then
                Call SearchExcel(File)
            End If
        End If
    Next
End Sub


Sub SearchExcel(File)

    Dim Flag
    
    Dim TmpBook
    Set TmpBook = Excel.Workbooks.Open(File)
    
    Dim TmpSheet
    For Each TmpSheet In TmpBook.Worksheets
        
        Dim SearchArea
        Set SearchArea = TmpSheet.Cells
        
        Dim FoundCell
        Set FoundCell = SearchArea.Find(SearchWord)
        
        If Not FoundCell Is Nothing Then
            Flag = False
            Call WriteBookAndSheet(TmpSheet,File,Flag)
            
            Dim FirstAddr
            FirstAddr = FoundCell.Address

            Do While True
                FoundCell.Activate
                Call WriteResult(FoundCell)
                
                Set FoundCell = SearchArea.FindNext(FoundCell)
                If FoundCell.Address = FirstAddr Then Exit Do
            Loop
            Call WriteBlank()
        End If
    Next
    
    TmpBook.Close
    
    Set TmpBook = Nothing
    Set TmpSheet = Nothing
    Set SearchArea = Nothing
    Set FoundCell = Nothing
    
End Sub

Sub WriteHeader()
    Range.Value = "検索文字列："
    Range.ColumnWidth = 20
    Range.Font.Bold = True
    Range.Interior.ColorIndex = 6
    Set Range = Range.Offset(0,1)
    Range.Value = SearchWord
    Range.ColumnWidth = 100
    Set Range = Range.Offset(2,-1)
End Sub

SUb WriteBookAndSheet(TmpSheet,File,Flag)
    If Flag = False Then
        Range.Value = "ファイル名："
        Range.Font.Bold = True
        Range.Interior.ColorIndex = 15
        Set Range = Range.Offset(0,1)
        Range.Value = fs.GetFile(File).Path
        Set Range = Range.Offset(1,-1)
        Flag = True
    End If
    Range.Value = "シート名："
    Range.Font.Bold = True
    Range.Interior.ColorIndex = 15
    Set Range = Range.Offset(0,1)
    Range.Value = TmpSheet.Name
    Set Range = Range.Offset(1,-1)
    Range.Value = "位置"
    Range.Font.Bold = True
    Range.Interior.ColorIndex = 15
    Set Range = Range.Offset(0,1)
    Range.Value = "内容"
    Range.Font.Bold = True
    Range.Interior.ColorIndex = 15
    Set Range = Range.Offset(1,-1)
End Sub

Sub WriteResult(FoundCell)
    Range.Value = FoundCell.Address
    Set Range = Range.Offset(0,1)
    Range.Value = FoundCell.Value
    Set Range = Range.Offset(1,-1)
End Sub

Sub WriteBlank()
    Set Range = Range.Offset(1,0)
End Sub
