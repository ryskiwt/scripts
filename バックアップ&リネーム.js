// 指定したバックアップ先フォルダにコピーを作成し、
// オリジナルのファイルを下記の名前にリネームします。
// [元のファイル名の'__'以前の部分]__YYYYMMDD-X.ext （YYYYMMDDはコピーした日付、Xは整数）
// もし、同名のファイルが存在する場合は、Xが異なる数値になるまでカウントアップします。

var numFiles = WScript.Arguments.Count();
if(numFiles==0) {
  WScript.Quit();
}

var originFile0      = WScript.Arguments.Item(0);
var originFoldPath = GetParentFoldPath(originFile0);
var backupFoldPath = GetBackupFoldPath(originFoldPath)

message1 = InitMessage('下記バックアップファイルを作成しました。',backupFoldPath);
message2 = InitMessage('同名ファイルが存在したため、下記ファイルの名前変更をスキップしました。',originFoldPath);

for( var i=0; i<numFiles; i++ ){
  originFilePath = WScript.Arguments.Item(i);

  var backupFileName = GenBackupFileName(originFilePath,backupFoldPath);
  var backupFilePath = backupFoldPath +'/'+ backupFileName;

  CopyFile(originFilePath,backupFilePath);
  message1 = AddMessage(message1,backupFileName);

  var newFileName = GenNewFileName(originFilePath);
  var newFilePath = originFoldPath +'/'+ newFileName
  var fs = new ActiveXObject( "Scripting.FileSystemObject" );
  if( fs.FileExists(newFilePath) ){
    message2 = AddMessage(message2,newFileName);
    continue;
  }
  fs.GetFile(originFilePath).Name = newFileName;
}

new ActiveXObject( "WScript.Shell" ).Popup(message1 +'\n'+ message2,0,'完了',48+0);




function GetBackupFoldPath(initPath) {
  var handle = 0;
  var title = 'バックアップ先のフォルダを指定してください';
  var opt = 16;
  var folder   = new ActiveXObject( "Shell.Application" )
                 .BrowseForFolder(handle,title,opt,initPath);
  if( folder == null ) {
    WScript.Quit();
  }
  return folder.Items().Item().Path;
}

function GetParentFoldPath(file){
  return new ActiveXObject( "Scripting.FileSystemObject" ).GetParentFolderName(file);
}

function InitMessage(message,path){
  message += '\n[フォルダ] ' +path+ '\n';
  return message;
}
function AddMessage(message,fileName){
  message += '    - ' +fileName+ '\n'
  return message;
}

function GenBackupFileName(originFile,backupFolderPath){
  var fs = new ActiveXObject( "Scripting.FileSystemObject" );
  var baseName = fs.GetBaseName(originFile);
  var extName  = fs.GetExtensionName(originFile);

  while(true){
    var backupFileName = baseName + '.' + extName;
    var backupFilePath = backupFolderPath + '/' + backupFileName;
    if( !fs.FileExists(backupFilePath) ){
      break;
    }
    baseName += 'a';
  }
  return backupFileName;
}

function CopyFile(srcPath,destPath){
  var override = false;
  new ActiveXObject( "Scripting.FileSystemObject" ).CopyFile(srcPath,destPath,override);
}

function GenNewFileName(originFile){
  var fs = new ActiveXObject( "Scripting.FileSystemObject" );
  var baseName = fs.GetBaseName(originFile);
  var extName  = fs.GetExtensionName(originFile);
  
  var splittedName = baseName.split('__');
  baseName = splittedName[0] + '__' + GenYYYYMMDD();
  return baseName + '.' + extName;
}

function GenYYYYMMDD() {
  var today = new Date();
  var retval = today.getYear();
  retval += Format2d(today.getMonth() + 1);
  retval += Format2d(today.getDate());
  return retval;
}

function Format2d(intValue) {
  if (intValue < 10) {
    return ("0" + intValue);
  } else {
    return (""  + intValue);
  }
}