Attribute VB_Name = "インポートと全削除"
Sub マクロモジュールのインポート()
    Dim Filepaths As Variant
    Filepaths = Application.GetOpenFilename( _
        FileFilter:="エクセルVBAファイル(*.bas),*.bas" & ",すべてのファイル(*.*),*.*", _
        FilterIndex:=1, _
        Title:="インポートするファイルを選択", _
        MultiSelect:=True)
        
    Dim Filepath As Variant
    For Each Filepath In Filepaths
        ThisWorkbook.VBProject.VBComponents.Import (Filepath)
    Next
End Sub

Sub マクロモジュールの全削除()
    For Each Component In ThisWorkbook.VBProject.VBComponents
        If Component.Type = 1 And Component.Name <> "インポートとエクスポート" Then
            ThisWorkbook.VBProject.VBComponents.Remove (Component)
        End If
    Next
End Sub
