Attribute VB_Name = "ハイライト置換"
Sub ハイライト置換()
    
    Dim SearchArea As Range
    Set SearchArea = Selection
    
    Dim InputString As String
    Dim TitleString As String
    Dim PromptString As String
    TitleString = "ハイライト置換"
    PromptString = "引数１に検索文字列、引数２に置換文字列を指定" & vbNewLine & _
                    "例： Search Word,New Word"
    InputString = Application.InputBox(PromptString, TitleString, Type:=2)
    If InputString = "False" Then Exit Sub
    
    Dim SplittedString As Variant
    Dim SearchWord As String
    Dim NewWord As String
    SplittedString = Split(InputString, ",")
    If UBound(SplittedString) <> 1 Then Exit Sub
    SearchWord = SplittedString(0)
    NewWord = SplittedString(1)
    
    Dim FoundCell As Range
    Set FoundCell = SearchArea.Find(What:=SearchWord, LookIn:=xlValues, _
        LookAt:=xlPart, SearchOrder:=xlByRows, MatchCase:=False, MatchByte:=False)
    If FoundCell Is Nothing Then Exit Sub
    
    Dim FirstAddr As String
    FirstAddr = FoundCell.Address
    
    Dim AutoFlag As Boolean
    Dim Response As Long
    
    Response = MsgBox("全て置換しますか？", vbYesNoCancel + vbQuestion, "ハイライト置換")
    If Response = 2 Then
        Exit Sub
    ElseIf Response = 6 Then
        AutoFlag = True
    ElseIf Response = 7 Then
        AutoFlag = False
    End If
    
    Do While True
        FoundCell.Activate
        Dim StartPos As Long
        If AutoFlag = False Then
            Response = MsgBox("置換してよろしいですか？", vbYesNoCancel + vbQuestion, "ハイライト置換")
        End If
            
        If Response = 2 Then
            Exit Sub
        ElseIf Response = 6 Then
            StartPos = InStr(1, FoundCell.Value, SearchWord, vbTextCompare)
            FoundCell.Characters(StartPos, Len(SearchWord)).Delete
            FoundCell.Characters(StartPos, 0).Insert (NewWord)
            FoundCell.Characters(StartPos, Len(NewWord)).Font.ColorIndex = 3
        ElseIf Response = 7 Then
        End If
        
        Set FoundCell = SearchArea.FindNext(After:=FoundCell)
        If FoundCell Is Nothing Then Exit Do
        If FoundCell.Address = FirstAddr Then Exit Do
    Loop
        
End Sub
