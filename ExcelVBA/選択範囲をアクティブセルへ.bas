Attribute VB_Name = "選択範囲をアクティブセルへ"
Sub 選択範囲をアクティブセルへ()
    Dim str As String
    str = ""
    Dim Cnt
    Cnt = 0
    
    Dim targetCell As Range
    For Each targetCell In Selection.Cells
        If targetCell.Value = "" Then
        ElseIf Cnt = 0 Then
            str = targetCell.Value
            Cnt = Cnt + 1
        Else
            str = str & vbNewLine & targetCell.Value
            Cnt = Cnt + 1
        End If
        targetCell.Value = ""
    Next
    ActiveCell.Value = str
End Sub
