Attribute VB_Name = "検索してハイライト"
Sub 検索してハイライト()
    
    Dim SearchArea As Range
    Set SearchArea = Selection
    
    Dim InputString As String
    Dim TitleString As String
    Dim PromptString As String
    TitleString = "検索してハイライト"
    PromptString = "引数１に検索文字列、引数２に背景色設定要否(Default:True)を指定" & vbNewLine & _
                    "例： Search Keyword[,Boolean]"
    InputString = Application.InputBox(PromptString, TitleString, Type:=2)
    If InputString = "False" Then Exit Sub
    
    Dim SplittedString As Variant
    Dim Target As String
    Dim BgColorFlag As Boolean
    SplittedString = Split(InputString, ",")
    Target = SplittedString(0)
    BgColorFlag = True
    If UBound(SplittedString) > 0 Then
        If SplittedString(1) = "False" Then BgColorFlag = False
    End If
    
    Dim FoundCell As Range
    Set FoundCell = SearchArea.Find(What:=Target, LookIn:=xlValues, LookAt:=xlPart, MatchCase:=False, MatchByte:=False)
    If FoundCell Is Nothing Then Exit Sub
    
    Dim FirstAddr As String
    FirstAddr = FoundCell.Address
    
    Do While True
        Dim StartPos As Long
        StartPos = InStr(1, FoundCell.Value, Target, vbTextCompare)
        With FoundCell.Characters(StartPos, Len(Target)).Font
            .ColorIndex = 3
        End With
        If BgColorFlag = True Then FoundCell.Interior.ColorIndex = 6
        
        Set FoundCell = SearchArea.FindNext(After:=FoundCell)
        If FoundCell Is Nothing Then Exit Do
        If FoundCell.Address = FirstAddr Then Exit Do
    Loop
        
End Sub
