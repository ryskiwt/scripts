Attribute VB_Name = "内容を保持して結合"
Sub 内容を保持して結合()
    Dim str As String
    str = ""
    Dim Cnt
    Cnt = 0
    
    Dim targetCell As Range
    For Each targetCell In Selection.Cells
        If targetCell.Value = "" Then
        ElseIf Cnt = 0 Then
            str = targetCell.Value
            Cnt = Cnt + 1
        Else
            str = str & vbNewLine & targetCell.Value
            Cnt = Cnt + 1
        End If
        targetCell.Value = ""
    Next
    ActiveCell.Value = str
    
    Selection.Merge
End Sub
